import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MainComponent } from './components/layouts/main/main.component';
import { GameComponent } from './components/layouts/game/game.component';
import { AppRoutingModule } from './app-routing/app-routing.module';

import { FormsModule } from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule, MatSelectModule} from '@angular/material';

import { TttService } from './components/widgets/ttt/services/ttt.service';

import { TttCellsComponent } from './components/widgets/ttt/ttt-cells/ttt-cells.component';
import { TttCellComponent } from './components/widgets/ttt/ttt-cell/ttt-cell.component';
import { TttIndexComponent } from './components/widgets/ttt/ttt-index/ttt-index.component';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    GameComponent,
    TttCellComponent,
    TttCellsComponent,
    TttIndexComponent,
  ],
  imports: [
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule, MatSelectModule
  ],
  providers: [TttService],
  bootstrap: [AppComponent]
})
export class AppModule { }
