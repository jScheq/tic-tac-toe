import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { MainComponent } from '../components/layouts/main/main.component';
import { GameComponent } from '../components/layouts/game/game.component';

const appRoutes: Routes = [
  {
    path: '', component: MainComponent
  },
  {
    path: 'game', component: GameComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
