import { Component, OnInit } from '@angular/core';
import {MatSelectModule} from '@angular/material';
import { TttService } from '../../widgets/ttt/services/ttt.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
})
export class MainComponent implements OnInit {

  fields: Array<{value: Number}>;
  selectedValue: string;

  constructor(private tttService: TttService) {
    this.fields = [
      {value: 5},
      {value: 10},
      {value: 20},
    ];
  }

  ngOnInit() {
  }

  getStarted() {
    this.tttService.setFieldSize(+this.selectedValue);
  }

}
