import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { ICell } from '../interfaces/i-cell';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Logic } from '../logic/logic';

@Injectable()
export class TttService {
  private status: number = null; // игровой статус
  private arrayOfCells: ICell[] = []; // здесь храним данные для изменения стрима cells$
  private cells$: Subject<ICell[]> = new Subject(); // стрим с состоянием игры
  private fieldSize$: BehaviorSubject<number> = new BehaviorSubject(5); // размер поля
  private status$: BehaviorSubject<number> = new BehaviorSubject(null); // результат для компонента
  private logic: Logic; // работает с пространством состояний игры, инкапсулирует бизнес-логику

  constructor() {
    this.logic = new Logic();
  }

  /**
   * Создаем ячейки и заполняем начальное состояние игрового поля
   *
   * @param fieldSize - размерность игрового поля, например, 5 для поля 5х5
   * @param cellWidth - ширина ячейки
   */
  initCells(fieldSize: number): void {
    // tslint:disable-next-line:no-shadowed-variable
    this.fieldSize$.subscribe((fieldSize: number) => {
      this.arrayOfCells = [];
      const cellWidth =  this.getBorder() / fieldSize;

      for (let i = 0; i < fieldSize * fieldSize; i++) {
        this.arrayOfCells.push({
          width: cellWidth,
          height: cellWidth,
          value: null,
          id: i,
        });
      }

      this.status$.next(null);
      this.cells$.next(this.arrayOfCells);
    });
  }

  /**
   * Обнуляет состояние игры
   */
  reloadGame(): void {
    for (let i = 0; i < this.arrayOfCells.length; i++) {
      this.arrayOfCells[i].value = null;
    }
    this.status = null;
    this.cells$.next(this.arrayOfCells);
  }

  /**
   * Геттер стрима с текущим состоянием игры
   */
  getCells(): Observable<ICell[]> {
    return this.cells$;
  }

  /**
   * Геттер Результата игры
   */
  getStatus(): Observable<number> {
    return this.status$;
  }

  /**
   * Геттер размерности поля
   */
  getFieldSize(): Observable<number> {
    return this.fieldSize$;
  }

  /**
   * Cеттер размерности поля
   */
  setFieldSize(value: number): void {
    this.fieldSize$.next(value || 5);
  }

  /**
   * Делает ход
   *
   * @param id - id ячейки
   */
  makeAMove(id: number) {
    if (this.status === null) {
      const preparedCells = this.prepareCellsToLogicCalc(this.arrayOfCells);

      this.cells$.next(this.logic.makeAMove(id, this.arrayOfCells));

      this.status = this.logic.checkResult(preparedCells);

      if (this.status !== null) {
        this.status$.next(this.status);
      }
    }
  }

  /**
   * Преобразует линейный массив к виду двемерной матрицы
   *
   * @param arrayOfCells - массив с текущим состоянием игры
   */
  private prepareCellsToLogicCalc(arrayOfCells: ICell[]): Array<Array<ICell>> {
    const n: number = Math.round(Math.sqrt(arrayOfCells.length));
    const matrix = new Array(n);

    // tslint:disable-next-line:no-shadowed-variable
    for (let i = 0; i < n; i++) {
      matrix[i] = new Array(n);
    }

    let q = 0;
    for (let i = 0; i < n; i++) {
      for (let j = 0; j < n; j++) {
        matrix[i][j] = arrayOfCells[q++];
      }
    }

    return matrix;
  }

  getBorder(): number {
    return window.screen.availHeight - 150;
  }

}
