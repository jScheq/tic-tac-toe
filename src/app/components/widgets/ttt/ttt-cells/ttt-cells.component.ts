import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { Subject } from 'rxjs/Subject';
import { ICell } from '../interfaces/i-cell';
import { TttService } from '../services/ttt.service';

@Component({
  selector: 'app-ttt-cells',
  templateUrl: './ttt-cells.component.html',
  styleUrls: ['./ttt-cells.component.scss'],
})
export class TttCellsComponent implements OnInit {
  test: string;
  border: number;
  status: number;
  fieldSize: number;
  cells: Array<ICell> = [];

  constructor(private tttService: TttService) {
    this.tttService
      .getCells()
      .subscribe((cells: ICell[]) => this.cells = cells);

    this.tttService
      .getStatus()
      .subscribe((status: number) => this.status = status);

    this.tttService
      .getFieldSize()
      .subscribe((fieldSize: number) => this.fieldSize = fieldSize);
  }

  ngOnInit() {
    this.border = this.tttService.getBorder() + 50;

    this.tttService.initCells(this.fieldSize);
  }

  whatAScore(): string {
    switch (this.status) {
      case -1:
        return 'Ничья';
      case 0:
        return 'Вы проиграли(';
      case 1:
        return 'Вы выиграли';
      default:
        return null;
    }
  }

  reloadGame(): void {
    this.status = null;
    this.tttService.reloadGame();
  }
}
