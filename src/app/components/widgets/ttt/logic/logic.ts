import { ICell } from '../interfaces/i-cell';
import { ILogic } from '../interfaces/i-logic';
import { Injectable } from '@angular/core';

@Injectable()
export class Logic implements ILogic {
  private arrayOfCells: ICell[];

  constructor() { }

  public makeAMove(id: number, arrayOfCells: ICell[]): ICell[] {
    this.arrayOfCells = arrayOfCells;

    if (this.checkPosibilityPlayerMove(id)) {
      this.makeAPlayerMove(id);
      this.makeAMachineMove();
    }

    return arrayOfCells;
  }

  public checkResult(cells: Array<Array<ICell>>): number {
    // TODO: отрефакторить вызов проверок
    // console.log(cells);
    if (this.checkTie(cells)) {
      return -1;
    }

    let result = this.checkHorizontal(cells);
    // console.log(`result H: ${result}`);

    if (result === null) {
      result = this.checkVertical(cells);
    }
    // console.log(`result V: ${result}`);

    if (result === null) {
      result = this.checkDiagonal(cells);
    }
    // console.log(`result D: ${result}`);

    return result;
  }

  /**
   * Сделать ход игроком
   *
   * @param id
   */
  private makeAPlayerMove(id: number): void {
    this.setValue(id, 1);
  }

  /**
   * Проверяем есть ли возможность сделать ход
   *
   * @param id
   */
  private checkPosibilityPlayerMove(id: number): boolean {
    for (let i = 0; i < this.arrayOfCells.length; i++) {
      if (this.arrayOfCells[i].id === id && this.arrayOfCells[i].value !== null) {
        return false;
      }
    }

    return true;
  }

  /**
   * 'ИИ' делает свой ход
   */
  private makeAMachineMove(): void {
    const ids = this.arrayOfCells
      .filter((cell: ICell) => cell.value === null)
      .map((cell: ICell) => cell.id);

    const machineMoveId = ids[Math.floor(Math.random() * ids.length)];

    this.setValue(machineMoveId, 0);
  }

  /**
   * Ставим ход
   *
   * @param id - id ячейки
   * @param type - значение: крестик или нолик
   */
  private setValue(id: number, type: number) {
    this.arrayOfCells.forEach(cell => {
      if (cell.id === id) {
        cell.value = type;
      }
    });
  }

  /**
   * Проверяем победу по горизонтали
   *
   * @param cells - ячейки
   */
  private checkHorizontal(cells: Array<Array<ICell>>): number {
    for (let i = 0; i < cells.length; i++) {
      let counterH = 0;
      const firstValueH: number = cells[i][0].value;
      for (let j = 0; j < cells.length; j++) {
        if (firstValueH !== null && firstValueH === cells[i][j].value) {
          counterH++;
        }
        if (counterH === cells.length) {
          return firstValueH;
        }
      }
    }
    return null;
  }

   /**
   * Проверяем победу по вертикали
   *
   * @param cells - ячейки
   */
  private checkVertical(cells: Array<Array<ICell>>): number {
    for (let i = 0; i < cells.length; i++) {
      let counterV = 0;
      const firstValueV: number = cells[0][i].value;
      for (let j = 0; j < cells.length; j++) {
        if (firstValueV !== null && firstValueV === cells[j][i].value) {
          counterV++;
        }
        if (counterV === cells.length) {
          return cells[j][i].value;
        }
      }
    }
    return null;
  }

   /**
   * Проверяем победу по диагоналям
   *
   * @param cells - ячейки
   */
  private checkDiagonal(cells: Array<Array<ICell>>): number {
    let counterL = 0;
    let counterR = 0;
    const firstValueL = cells[0][0].value;
    const firstValueR = cells[cells.length - 1][0].value;

    for (let i = 0; i < cells.length; i++) {
      if (firstValueL !== null && firstValueL === cells[i][i].value) {
        counterL++;
      }
      if (firstValueR !== null && firstValueR === cells[i][cells.length - i - 1].value) {
        counterR++;
      }
    }

    if (counterL === cells.length) {
      return firstValueL;
    }
    if (counterR === cells.length) {
      return firstValueR;
    }
    return null;
  }

  /**
   * Проверка на ничью
   *
   * @param cells
   */
  private checkTie(cells: Array<Array<ICell>>): boolean {
    let counter = 0;
    for (let i = 0; i < cells.length; i++) {
      for (let j = 0; j < cells.length; j++) {
        if (cells[i][j].value !== null) {
          counter++;
        }
      }
    }

    return counter === Math.pow(cells.length, 2);
  }

}
