import { Component, OnInit, Input } from '@angular/core';
import { ICell } from '../interfaces/i-cell';
import { TttService } from '../services/ttt.service';

@Component({
  selector: 'app-ttt-cell',
  templateUrl: './ttt-cell.component.html',
  styleUrls: ['./ttt-cell.component.scss'],
})
export class TttCellComponent implements OnInit {

  @Input() cell: ICell;

  constructor(private tttService: TttService) { }

  ngOnInit() {

  }

  click() {
    this.tttService.makeAMove(this.cell.id);
  }

}
