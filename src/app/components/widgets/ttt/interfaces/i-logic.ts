import { ICell } from './i-cell';

export interface ILogic {
    /**
     * Делает ход игроком если он возможен, делает ход ИИ
     * и возвращает текущее состояние игры
     *
     * @param id - id клетки
     * @param arrayOfCells - массив ячеек
     */
    makeAMove(id: number, arrayOfCells: ICell[]): ICell[];

    /**
     * Проверяем не окончилась ли игра
     *
     * @param cells - двумерная матрица состояния игры
     */
    checkResult(cells: Array<Array<ICell>>): number;
}
