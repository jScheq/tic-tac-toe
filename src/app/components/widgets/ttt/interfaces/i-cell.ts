export interface ICell {
    id: number;
    width: number;
    height: number;
    value?: number;
}
